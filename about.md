---
layout: page
title: About
---

<img src="/images/edouard_briere.jpg" class="avatar" alt="Photo of Édouard Brière">

I’m Édouard Brière, and I’m a web developer currently based in Nantes, France.

From 2007 through 2009 I worked as a web developer and internationalization engineer for [Last.fm](http://last.fm), a social music platform based in United Kingdom.

I also lived for about 3 years in Sweden and I hold a Masters Degree in Computer Sciences from the [University of Linköping](http://en.wikipedia.org/wiki/Linköping_University).

I currently work on [WebTranslateIt.com](https://webtranslateit.com) which is a translation software I created about 6 years ago, in 2009.

— Autumn 2014

---